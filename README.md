<h1>Google Tag Manager for javascript apps</h1>
<p>Easily add GTM tracking to your app</p>

<h2>How to use:</h2>
<ul>
    <li>Run npm install --save google_tag</li>
    <li>Add GoogleTag.addTag(your_gtm_id); in your function.</li>
</ul>