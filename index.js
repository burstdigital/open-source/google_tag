function GoogleTag() {

}

GoogleTag.addTag = function(gtm_id, dataLayer) {

  if (!gtm_id) {
    return console.warn('GoogleTag:', 'No GTM ID supplied');
  }

  dataLayer = dataLayer || 'dataLayer';

  var scripts = document.querySelectorAll('script[data-tag="gtm"]') || [];

  for (var i = 0; i < scripts.length; i++) {

    if (scripts[i].src.indexOf('gtm.js') !== -1) {
      return scripts[i].src = '//www.googletagmanager.com/gtm.js?id=' + gtm_id;
    }
  }

  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.setAttribute('data-tag', 'gtm');j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script',dataLayer,gtm_id);
}

module.exports = GoogleTag;